// @author : Arun <arun@uni-bremen.de>
//
// Print the different bases of the numbers of aribtrary length. (well max 1024 bits)
//
// More robust form of the shell function
// base () {
//	setopt LOCAL_OPTIONS C_BASES OCTAL_ZEROES
//	printf "decimal %s       =       %08d       0x%x\n" $1 ${$(([#2] $1))#2\#} $(($1))
// }

#include <iostream>
#include <string>
#include <ctype.h>
#include <locale>
#include <algorithm>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <cstdlib>

using mpuint = boost::multiprecision::uint1024_t;
namespace
{

const std::string usage = "[i] Usage: base <number>     (Prefix 0x or 0b to denote hex/binary)\n";

//---------------------------------------------------------------------------------------
inline bool is_binary(const std::string &str)
{
  if (str[0] != '0') return false;
  if (str[1] != 'b' && str[1] != 'B') return false;
  for (const char &i: str)
  {
    if (i != 'b' && i != '0' && i != '1' && i != 'B')
    {
      return false;
    }
  }
  return true;
}

inline bool is_decimal(const std::string &str)
{
  for (const char &i: str)
  {
    if (!std::isdigit(i))   return false;
  }
  return true;
}

inline bool is_hexadecimal(const std::string &mystr)
{
  if (mystr[0] != '0') return false;
  if (mystr[1] != 'x' && mystr[1] != 'X') return false;
  auto str = mystr.substr(2);
  for (const char &i: str)
  {
    if (!std::isxdigit(i))  return false;
  }
  return true;
}

//---------------------------------------------------------------------------------------

// callees responsibility to check if the input string is valid or not.
// for binary, decimal and hexadecimal.
mpuint str_to_dec(const std::string &mystr, const int konst)
{
  auto str = boost::to_lower_copy(mystr, std::locale());
  mpuint num = 0;
  for (char &i : str)
  {
    if (konst == 2 && i == 'b') continue;
    switch (i)
    {
    case 'x':   continue;
    case 'a':   num = num * konst + 10;    break;
    case 'b':   num = num * konst + 11;    break;
    case 'c':   num = num * konst + 12;    break;
    case 'd':   num = num * konst + 13;    break;
    case 'e':   num = num * konst + 14;    break;
    case 'f':   num = num * konst + 15;    break;
    default:
      num = num * konst + (int)(i - '0');
    }
  }
  return num;
}

//---------------------------------------------------------------------------------------
std::string mpuint_to_binary_string (const mpuint &number, const int limit)
{
  std::string numstr;
  int cnt = 0;
  auto num = number;
  while (num > 0)
  {
    auto bit = ((num >> 1) << 1) ^ num; 
    if (bit == 0)  // note: bit is mpuint, cant convert to string
    {
      numstr.append("0"); 
    }
    else 
    {
    numstr.append({"1"}); 
    }
    num = num >> 1;
    if (++cnt >= limit)  // enough digits.
    {
      std::cout << "[w] Truncating the number of binary digits printed to first " 
                << limit << "\n";
      break; 
    } 
  }
  return std::string(numstr.rbegin(), numstr.rend());
}
//---------------------------------------------------------------------------------------

}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

int main(int argc, char **argv)
{
  if (argc == 1 || argc > 2)
  {
    std::cout << usage;
    return 0;
  }

  mpuint num;

  auto str = std::string(argv[1]);
  if ( is_binary(str) )             num = str_to_dec (str, 2);
  else if ( is_decimal(str) )       num = str_to_dec (str, 10);
  else if ( is_hexadecimal(str) )   num = str_to_dec (str, 16);
  else
  {
    std::cout << "[e] Cannot parse the input number: " << str << "\n";
    std::cout << usage;
    std::exit (-3);
  }

  std::cout << "Dec = " << std::dec << num << "\n";
  std::cout << "Hex = " << std::hex << num << std::dec << "\n";
  auto bin_str = mpuint_to_binary_string(num, 512);
  std::cout << "Bin = " << bin_str 
            << "   (" << bin_str.size() << " bits unsigned)" << "\n";

  return 0;
}





//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
// Old depreciated helper functions.
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

int print_with_bs (mpuint &num) 
{
  boost::dynamic_bitset<> bs;
  unsigned cnt = 0u;
  const unsigned limit = 128;
  while (num > 0)
  {
    if (cnt >= limit)
      break;
    auto bit = ((num >> 1) << 1) ^ num;
    if (0 == bit)
      bs.push_back(0);
    else
      bs.push_back(1);
    num = num >> 1;
    cnt++;
  }

  std::cout << std::dec;
  std::cout << "Bin = ";
  if (cnt < limit)
  {
    for (auto i = bs.size(); i > 0; i--)
      std::cout << bs[i - 1];
    std::cout << "   (" << cnt << " bits unsigned)\n";
    return 0;
  }

  std::cout << " Im NOT going to print binaries for more than " << limit << " bits!\n";
  return 0;
}

mpuint get_number_lex(const std::string &str)
{
  mpuint num;
  try
  {
    num = boost::lexical_cast<mpuint>(str);
  }
  catch (boost::bad_lexical_cast &)
  {
    std::cout << "[e] Is " << str << " a valid number?\n";
    std::cout << "[i] Usage: base [-b] <number>  (Use -b to interpret number as binary)\n";
    exit(-2);
  }
  return num;
}

mpuint get_number_bs(const std::string &str)
{
  if (!std::all_of(str.begin(), str.end(), ::isdigit))
  { // not even a digit!
    std::cout << "[e] Is " << str << " a valid binary number?\n";
    std::cout << "[i] Usage: base [-b] <number>  (Use -b to interpret number as binary)\n";
    exit(-3);
  }
  if (str.find_first_of("23456789") != std::string::npos)
  { // not binary.
    std::cout << "[e] Is " << str << " a valid binary number? (Try without -b)\n";
    std::cout << "[i] Usage: base [-b] <number>  (Use -b to interpret number as binary)\n";
    exit(-3);
  }

  boost::dynamic_bitset<> bs(str); // never throws excpetion. Only crashes
  return mpuint(bs.to_ulong());
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:



SHELL=/bin/bash
base: main.cpp
	g++ -Wall -std=c++11 main.cpp -o base

install: base
	cp -f base /usr/bin/

clean:
	rm -f base


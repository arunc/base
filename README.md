Base: Print the different bases for numbers of aribtrary length
================================================================

Print the decimal, binary and hexadecimal values of the number entered.
The number can be arbitrarily long (1024 bits is the limit).
All numbers are taken as unsigned.

An enhanced arbitrary precision form of the ZSH/BASH shell function
```shell
base () {
setopt LOCAL_OPTIONS C_BASES OCTAL_ZEROES
printf "decimal %s       =       %08d       0x%x\n" $1 ${$(([#2] $1))#2\#} $(($1))
}
```

> ### Author: Arun <arun@uni-bremen.de>

How to build and run?
---------------------

### Dependencies:

* BOOST Libraries ( tried only with version 1.61 )
* C++11 compiler  ( Makeflow uses GCC, and targets a Linux system )
* Gnu Make  ( tried only in 4.1 )

### Build steps:
* make
* sudo make install

### Usage:
* ./base 0b1000  # Binary 
* ./base 1234    # Decimal 
* ./base 0x12fe  # Hexadecimal

```
shell-$ base 0x1234567890      
Dec = 78187493520
Hex = 1234567890
Bin = 1001000110100010101100111100010010000   (37 bits unsigned)
```
